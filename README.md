# ubuntu20.04-init

Ansible Repo that utilizes an ansible-pull command to configure my workstations.

## Status

Living breathing initialization script to setup ubuntu 20.04.  

Stolen from https://github.com/bashfulrobot/bashfulrobot-ansible - I would use his as your starting point, he knows what he is doing.

## My Workflow

```
bash
git clone git@gitlab.com:jasonrob/slutbox-init.git
cd slutbox-init
bash deploy.sh
```

## TODO

* generate ssh pub/priv key
* setup systemd timers for scheduled jobs
* get tigervnc w/ openbox setup to completion
* add dialog options for iptables rebuild
* discord apt / not sure if they have repo
* deploy user.js file to disable pocket - https://developer.mozilla.org/en-US/docs/Mozilla/Preferences/A_brief_guide_to_Mozilla_preferences
* Give flameshot a try, was in bashfulrobot repo looks promising