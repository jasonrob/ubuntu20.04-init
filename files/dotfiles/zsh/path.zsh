# You can put files here to add functionality separated per file, which
# will be ignored by git.
# Files on the custom/ directory will be automatically loaded by the init
# script, in alphabetical order.

# Add example bin
#export PATH=/path/to/bin:$PATH

# Add PIP binaries
export PATH=$HOME/.local/bin:$HOME/bin:$HOME/scripts:$PATH

# Set editor
export EDITOR='vim'

# Intermapper JAVA
export INTERMAPPER_JAVA='/opt/openjdk/bin/java'