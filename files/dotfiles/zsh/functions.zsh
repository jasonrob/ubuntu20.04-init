# You can put files here to add functionality separated per file, which
# will be ignored by git.
# Files on the custom/ directory will be automatically loaded by the init
# script, in alphabetical order.

function update-system-cfg() {
  APULL=$(which ansible-pull)
  MYREPORMT="https://gitlab.com/jasonrob/ubuntu20.04-init.git"
  $APULL -U $MYREPORMT
}

function cht() {
        WHICH=$(which curl)
        $WHICH -Ls cht.sh/"$1" | ${PAGER:-less -R};
}
